<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio (){
        return view('form');
    }
    public function welcome (Request $request){
        $nama = $request['nama'];
        $lastname = $request['lastname'];
        return view('welcome', compact('nama','lastname'));
    }
}
