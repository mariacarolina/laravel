@extends('layout.master')
@section('title')
    Halaman Pengisian Formulir
@endsection
@section('content')
    <h2>Buat Account Baru</h2>
    <h4>Sign Up Form</h4>
        <form action="/welcome" method="post">
            @csrf
            <label>First Name :</label><br><br>
            <input type="text" name="nama"><br><br>
            <label>Last Name :</label><br><br>
            <input type="text" name="lastname"><br><br>
            <label>Gender</label><br><br>
            <input type="radio" name="gender">Male <br>
            <input type="radio" name="gender">Female <br><br>
            <label>Nasionality</label><br><br>
            <select name="Nasionality">
                <option value="3">Indonesia</option>
                <option value="3">Amerika</option>
                <option value="4">Inggris</option>
            </select><br><br>
            <label>Language Spoken</label><br><br>
            <input type="checkbox" name="Language">Bahasa Indonesia <br>
            <input type="checkbox" name="Language">English <br>
            <input type="checkbox" name="Language">Other <br><br>
            <label>Bio</label><br><br>
            <textarea name="bio" cols="30" rows="10"></textarea><br>
            <input type="Submit" value="Sign Up">
        </form>
@endsection